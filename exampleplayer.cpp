#include "exampleplayer.h"
#include <stdio.h>

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
ExamplePlayer::ExamplePlayer(Side side) {

    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds to do stuff.
     */
    epboard = new Board();
    myside = side;
    if (myside == WHITE)
    {
		opponentside = BLACK;
	}
	else
	{
		opponentside = WHITE;
	} 
}

/*
 * Destructor for the player.
 */
ExamplePlayer::~ExamplePlayer() {
}


/*
 * Compute the next move given the opponent's last move. Each AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * If there are no valid moves for your side, doMove must return NULL.
 *
 * Important: doMove must take no longer than the timeout passed in 
 * msLeft, or your AI will lose! The move returned must also be legal.
 */
Move *ExamplePlayer::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */
    epboard->doMove(opponentsMove, opponentside);
    Move *move;
    int weight = -100000;
    if (epboard->hasMoves(myside))
    {
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) { //goes through all the cells to see which one would work
				if(epboard->checkMove( new Move(i, j), myside))
				{
					Move *move1st = new Move(i, j);
					Board *Board1st = epboard->copy();
					int countingweight = Board1st->count(myside) - Board1st->count(opponentside);
					Board1st->doMove(move1st, myside);
					int i1 = move1st->getX();
					int j1 = move1st->getY();
					// taking care of the positive weighted horizontal edges (including corners)
					if ((j1 == 0 || j1 == 7) && (i1 == 0 || i1 == 2 || i1 == 3
					|| i1 == 4 || i1 == 5 || i1 == 7))
					{
						countingweight += 5;
					}
					// taking care of the positive weighted vertical edges (including corners)
					if ((i1 == 0 || i1 == 7) && (j1 == 0 || j1 == 2 || j1 == 3
					|| j1 == 4 || j1 == 5 || j1 == 7))
					{
						countingweight += 5;
					}
					// taking care of the negative weighted squares that are directly adjacent to the corners and not
					// diagonal to the corners
					if ((i1 == 1 && j1 == 0) || (i1 == 0 && j1 == 1) ||
						(i1 == 1 && j1 == 7) || (i1 == 0 && j1 == 6) ||
						(i1 == 6 && j1 == 0) || (i1 == 6 && j1 == 7) ||
						(i1 == 7 && j1 == 1) || (i1 == 7 && j1 == 6))
						{
							countingweight -= 20;
						}
					// taking care of the negative weighted squares that are directly diagonal to the corners
					if ((i1 == 1 && j1 == 1) || (i1 == 6 && j1 == 1) ||
						(i1 == 1 && j1 == 6) || (i1 == 6 && j1 == 6))
						{
							countingweight -= 40;
						}
					// if the number of moves for opponent is smaller than or equal to 1, large positive weight
					if (1 >= Board1st->numofMoves(opponentside))
					{
						countingweight += 60;
					}
					// if the number of moves for opponent is larger than 1, large negative weight
					if (1 < Board1st->numofMoves(opponentside))
					{
						countingweight -= 60;
					}
					if (countingweight >= weight)
					{
						weight = countingweight;
						move = new Move(i, j);
						delete Board1st;
						delete move1st;
					}
					countingweight = 0;
				}
			}
        }
        epboard->doMove(move, myside);
		return move;
    }
	else
	{
		return NULL;
	}
}
