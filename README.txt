I worked solo.

I created an AI that was based on heuristics. It basically gave a very large negative weight (-40) to squares with position
(1, 6), (1, 1), (6, 1), and (1, 1), a medium negative weight (-20) to the squares that are adjacent (below/left/right/above)
to the corners (the positions are (1, 0), (0, 1), (1, 7), (0, 6), (6, 0), (6, 7), (7, 1), (7, 6)) and positive weights (+ 5)
to the squares that are along the edges that are not the corners or adjacent to corners (the positions are (0, 2), (0, 3)
(0, 4), (0, 5), (7, 2), (7, 3), (7, 4), (7, 5), (2, 0), (3, 0), (4, 0), (5, 0), (2, 7), (3, 7), (4, 7), (5, 7)). Larger positive
weights (+10, since 5+5 is 10) were given to the squares that were at the corners (the positions are (0, 0), (0, 7), (7, 0), (7, 7))

I also created a heuristic where there was a very large negative weight (-60) if the opposing side had more than one possible move, and
a very large positive weight (+60) if the opposing side had 1 possible move or less. This was done by creating a numofMoves in the
Board class. 

This gave an AI which usually defeated Constant Time since it would try to choose the more positive weighted squares.

I attempted to do a minmax AI, but it didn't work too well. I tried to treat the game so that my side was choosing the best possible
score, and then choosing the worse possible score (which is the best possible score for the opponent). Here is my attempted code:

int totalsteps = 2;

int steps = 0;

int MySide(Board *Board1st; steps) {
    if (Board1st->isDone() || steps>totalsteps)
        {
			int i1 = move2nd->getX();
			int j1 = move2nd->getY();
			// taking care of the positive weighted horizontal edges (including corners)
			if ((j1 == 0 || j1 == 7) && (i1 == 0 || i1 == 2 || i1 == 3
			|| i1 == 4 || i1 == 5 || i1 == 7))
			{
				countingweight += 5;
			}
			// taking care of the positive weighted vertical edges (including corners)
			if ((i1 == 0 || i1 == 7) && (j1 == 0 || j1 == 2 || j1 == 3
			|| j1 == 4 || j1 == 5 || j1 == 7))
			{
				countingweight += 5;
			}
			// taking care of the negative weighted squares that are directly adjacent to the corners and not
			// diagonal to the corners
			if ((i1 == 1 && j1 == 0) || (i1 == 0 && j1 == 1) ||
				(i1 == 1 && j1 == 7) || (i1 == 0 && j1 == 6) ||
				(i1 == 6 && j1 == 0) || (i1 == 6 && j1 == 7) ||
				(i1 == 7 && j1 == 1) || (i1 == 7 && j1 == 6))
			{
				countingweight -= 20;
			}
			// taking care of the negative weighted squares that are directly diagonal to the corners
			if ((i1 == 1 && j1 == 1) || (i1 == 6 && j1 == 1) ||
				(i1 == 1 && j1 == 6) || (i1 == 6 && j1 == 6))
			{
				countingweight -= 40;
			}
			// if the number of moves for opponent is smaller than or equal to 1, large positive weight
			if (1 >= Board1st->numofMoves(opponentside))
			{
				countingweight += 60;
			}
			// if the number of moves for opponent is larger than 1, large negative weight
			if (1 < Board1st->numofMoves(opponentside))
			{
				countingweight -= 60;
			}
			delete Board2nd;
			delete move2nd;
			countingweight = 0;
    int weight1 = -10000;
    if (Board1st->hasMoves(myside))
    {
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) { //goes through all the cells to see which one would work
				if(epboard->checkMove( new Move(i, j), myside))
				{
					Board *Board2nd = Board1st->copy();
					Move *move2nd = new Move(i, j);
					Board2nd->doMove(move2nd, myside);
					int countingweight = OpponentSide(Board2nd, steps += 1);
					if (countingweight>weight1) 
					{
						weight1 = countingweight;
					}
				}
			}
		}
    }
    return weight1;
}
int OpponentSide(Board *Board1st; steps) {
     if (Board1st->isDone() || steps>totalsteps)
        {
			int i1 = move2nd->getX();
			int j1 = move2nd->getY();
			if ((j1 == 0 || j1 == 7) && (i1 == 0 || i1 == 2 || i1 == 3
			|| i1 == 4 || i1 == 5 || i1 == 7))
			{
				countingweight += 5;
			}
			if ((i1 == 0 || i1 == 7) && (j1 == 0 || j1 == 2 || j1 == 3
			|| j1 == 4 || j1 == 5 || j1 == 7))
			{
				countingweight += 5;
			}
			if ((i1 == 1 && j1 == 0) || (i1 == 0 && j1 == 1) ||
				(i1 == 1 && j1 == 7) || (i1 == 0 && j1 == 6) ||
				(i1 == 6 && j1 == 0) || (i1 == 6 && j1 == 7) ||
				(i1 == 7 && j1 == 1) || (i1 == 7 && j1 == 6))
			{
				countingweight -= 20;
			}
			if ((i1 == 1 && j1 == 1) || (i1 == 6 && j1 == 1) ||
				(i1 == 1 && j1 == 6) || (i1 == 6 && j1 == 6))
			{
				countingweight -= 40;
			}
			if (1 >= Board1st->numofMoves(opponentside))
			{
				countingweight += 60;
			}
			if (1 < Board1st->numofMoves(opponentside))
			{
				countingweight -= 60;
			}
			delete Board2nd;
			delete move2nd;
			countingweight = 0;
    int weight2 = 10000;
 if (Board1st->hasMoves(myside))
    {
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) { //goes through all the cells to see which one would work
				if(epboard->checkMove( new Move(i, j), myside))
				{
					Board *Board2nd = Board1st->copy();
					Move *move2nd = new Move(i, j);
					Board2nd->doMove(move2nd, myside);
					int countingweight = MySide(Board2nd, steps += 1);
					if (countingweight<weight2)
					{
						weight2 = countingweight;
					}
				}
			}
		}
	}
    return weight2;
}




